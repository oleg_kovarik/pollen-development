#!/usr/bin/env python
# coding: utf-8

"""Pollen segmentation and classification

Script extracts stained pollen grains from the light microscope images.
Extracted objects are classified as a developed/not developed classes.

Input:
	Expected directory structure: ./data/[temperature]/[id]/[*.jpg]
    Example:                      ./data/30/30_2/13_30_025_13 (4).jpg

Output:
	./output/objects/	all extracted objects sorted by class
	./output/[temperature]/[id]/	analyzed images + txt info
	./output/total.txt	info on all images

In addition to main classes 1 & 2, there are several classes with
problematic objects, which should be reviewed and used for parameter
tuning.

Classes:
	0 red		not used
	
	1 blue		DEVELOPED
	2 green		NOT DEVELOPED
	
	3 yellow	object too small
	4 purple	object too large
	5 cyan		object too asymmetric
	
	6, 7, 8, 9 black	should be class 2 (color based rules)
	
	10			not used
	11 white	should be class 1 (color based rules)

Parameter tuning can be done by changing threshold values in methods:

	apply_color_rules
	apply_diameter_rules
	save_object_images (asymmetry)
"""

import os
import pipes
import cv2
import numpy as np
import sys
import ntpath
import random
import statistics as stat
import matplotlib.pyplot as plt
from scipy.stats import itemfreq
import shutil

def cv_version():
    return cv2.__version__

class DetectedObject():
    def __init__(self, x, y, height, width, radius, objtype, r, g, b, h, s, v):
        self.x = x
        self.y = y
        self.height = height
        self.width = width
        self.radius = radius
        self.objtype = objtype
        self.r = r
        self.g = g
        self.b = b
        self.h = h
        self.s = s
        self.v = v
        self.asymmetry = -1

class RGB():
    def __init__(self, r, g, b):
        self.r = r
        self.g = g
        self.b = b
        
class HSV():
    def __init__(self, h, s, v):
        self.h = h
        self.s = s
        self.v = v


def Kmeans(img):
    """Perform K-means on image pixels
    
    Can be used in preprocess stage to reduce size of output images.
    
    Args:
        img: BGR image
    
    Returns:
        res2: BGR image, result of K-means algorithm 
    """
        
    Z = img.reshape((-1,3))
    
    # convert to np.float32
    Z = np.float32(Z)
    
    # define criteria, number of clusters(K) and apply kmeans()
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 10
    ret,label,center=cv2.kmeans(Z,K,None,criteria,10,cv2.KMEANS_RANDOM_CENTERS)
    
    # Now convert back into uint8, and make original image
    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))
    
    return res2

def white_balance_channel(channel, perc = 0.05):
    """White balance one image channel
    
    Args:
        channel: one channel of image
        perc: percentile of pixel values to clip from both sides
    
    Returns:
        channel: white balanced channel
    """
        
    mi, ma = (np.percentile(channel, perc), np.percentile(channel,100.0-perc))
    channel = np.uint8(np.clip((channel-mi)*255.0/(ma-mi), 0, 255))
    return channel

def white_balance(image, perc=0.5):
    """White balance image, each channel separately
    
    Args:
        image: bgr image
        perc: percentile of pixel values to clip from channels
        
    Returns:
        imageout: white balanced image
    """
    
    blue,green,red = cv2.split(image)
    imageout = cv2.merge([white_balance_channel(channel, perc) for channel in (blue, green, red)])
    return imageout

def get_mean_object_rgb_color(image, cx, cy, rad):
    """Calculate mean RGB values for circular object in image
    
    Args:
        image: BGR image
        cx: position of center on x axis
        cx: position of center on y axis
        rad: radius of object

    Returns:
        [r, g, b]: average values of Red, Green and Blue
    """
    cnt = r = g = b = 0
    height, width = image.shape[:2]
    for x in range(max(0, cx-rad), min(cx+rad, width-1)):
        for y in range(max(0, cy-rad), min(cy+rad, height-1)):
            if ((cx-x)*(cx-x)+(cy-y)*(cy-y) <= rad*rad):
                r = r + image[y, x][2]
                g = g + image[y, x][1]
                b = b + image[y, x][0]
                cnt += 1
    return [int(r/cnt), int(g/cnt), int(b/cnt)]

def get_mean_object_hsv_color(image, cx, cy, rad):
    """Calculate mean HSV values for circular object in image
    
    Args:
        image: HSV image
        cx: position of center on x axis
        cx: position of center on y axis
        rad: radius of object

    Returns:
        [h, s, v]: average values of Hue, Saturation and Value
    """

    cnt = h = s = v = 0
    height, width = image.shape[:2]
    for x in range(max(0, cx-rad), min(cx+rad, width-1)):
        for y in range(max(0, cy-rad), min(cy+rad, height-1)):
            if ((cx-x)*(cx-x)+(cy-y)*(cy-y) <= rad*rad):
                h = h + image[y, x][0]
                s = s + image[y, x][1]
                v = v + image[y, x][2]
                cnt += 1
    return [int(h/cnt), int(s/cnt), int(v/cnt)]

def get_dominant_image_color(image):
    """Calculate dominant image color
    
    Args:
        image: RGB image

    Returns:
        (domR, domG, domB): values of dominant color (Reg, Green and Blue)
    """
    
    average_color = [image[:, :, i].mean() for i in range(image.shape[-1])]
    arr = np.float32(image)
    pixels = arr.reshape((-1, 3))
    n_colors = 5
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 200, .1)
    flags = cv2.KMEANS_RANDOM_CENTERS
    _, labels, centroids = cv2.kmeans(pixels, n_colors, None, criteria, 10, flags)
    
    palette = np.uint8(centroids)
    quantized = palette[labels.flatten()]
    quantized = quantized.reshape(image.shape)
    
    dominant_color = palette[np.argmax(itemfreq(labels)[:, -1])]

    return RGB(int(dominant_color[2]), int(dominant_color[1]), int(dominant_color[0]))

def get_asymmetry(image, margin):
    """Calculate asymmetry of object

    Cut edges from image, resize to "size", threshold image and sum
    differences between medially symetric pixels.
     
    Args:
        image: RGB image
        margin: ignored margin

    Returns:
        diff: difference from ideal symmetry
    """

    size = 50
    
    height, width = image.shape[:2]
    subimg = image[margin:width-margin, margin:height-margin]

    image50 = cv2.resize(image,(size, size), interpolation = cv2.INTER_CUBIC)
    hsv = cv2.cvtColor(image50, cv2.COLOR_BGR2HSV)
    th, bw = cv2.threshold(hsv[:, :, 2], 0, 1, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
    
    diff = 0
    for x in range(0, size):
        for y in range(0, int(size/2)):
            d = int(bw[x][y]) - int(bw[size-x-1][size-y-1])
            diff += abs(d)
    return diff

def preprocess_image(origimg):
    """Preprocess image"""

    img = origimg.copy()
    
    # K-means can be used to reduce number of color shades resulting in
    # smaller output images
    
    #origimg = Kmeans(origimg)
    
    wbimg = white_balance(origimg)
        
    return img, wbimg

def extract_mask(img):
    """Extract mask
    
    Create mask from sample image - threshold in HSV space, fill contours, remove scale. 
    """
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    th, bw = cv2.threshold(hsv[:, :, 2], 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)
    
    # find contours of objects in thresholded image, draw them with parameter -1 -> filled
    kernel = np.ones((3, 3), np.uint8)
    
    if cv_version().startswith('3.'):
        _,contour,hier = cv2.findContours(bw,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)
    elif cv_version().startswith('4.'):
        contour,hier = cv2.findContours(bw,cv2.RETR_CCOMP,cv2.CHAIN_APPROX_SIMPLE)

    for cnt in contour:
        cv2.drawContours(bw,[cnt],0,255,-1)
    
    # mask out scale in bottom right corner
    for i in range(1420,1420+145):
        for j in range(1132,1132+60):
            bw[j][i] = 0
    
    return bw, hsv

def extract_contours(bw):
    """Extract contours
    
    Isolate circular objects (improve segmenation of overlapping objecs),
    create circular template and highlight matching areas in image,
    find peaks in intensity by thresholding and find contours
    """
    
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    morph = cv2.morphologyEx(bw, cv2.MORPH_CLOSE, kernel)
    dist = cv2.distanceTransform(morph, cv2.DIST_L2, 5 )

    # create template    
    borderSize = 20
    distborder = cv2.copyMakeBorder(dist, borderSize, borderSize, borderSize, borderSize, 
                                    cv2.BORDER_CONSTANT | cv2.BORDER_ISOLATED, 0)
    gap = 3                         
    kernel2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2*(borderSize-gap)+1, 2*(borderSize-gap)+1))
    kernel2 = cv2.copyMakeBorder(kernel2, gap, gap, gap, gap, 
                                    cv2.BORDER_CONSTANT | cv2.BORDER_ISOLATED, 0)
    #distTempl = cv2.distanceTransform(kernel2, cv2.DIST_L2, cv2.CV_DIST_MASK_PRECISE)
    distTempl = cv2.distanceTransform(kernel2, cv2.DIST_L2, 0)
    
    # match template
    nxcor = cv2.matchTemplate(distborder, distTempl, cv2.TM_CCOEFF_NORMED)
    
    # locate peaks
    mn, mx, _, _ = cv2.minMaxLoc(nxcor)
    th, peaks = cv2.threshold(nxcor, mx*0.7, 255, cv2.THRESH_BINARY)
    peaks8u = cv2.convertScaleAbs(peaks)
    if cv_version().startswith('3.'):
        _, contours, hierarchy = cv2.findContours(peaks8u, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    elif cv_version().startswith('4.'):
        contours, hierarchy = cv2.findContours(peaks8u, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    peaks8u = cv2.convertScaleAbs(peaks)    # to use as mask

    return dist, contours, peaks8u

def extract_objects(contours, dist, peaks8u, img, hsv):
    """Extract pollen objects from image
    
    Detect objects from dist and peaks8u. Ignore small objects.
    Basic classification: 1 - developed, 2 - not developed (too greeen)
    """

    # 1. pass trhough detected object- calculate features
    objects = []
    sumr = sumg = sumb = 0
    sumh = sums = sumv = 0
    for i in range(len(contours)):
        x, y, w, h = cv2.boundingRect(contours[i])

        # ignore empty object
        if (w==0) or (h==0): continue
        
        # extract object coordinates and diameter
        _, mx, _, mxloc = cv2.minMaxLoc(dist[y:y+h, x:x+w], peaks8u[y:y+h, x:x+w])
        cx, cy = [int(mxloc[0]+x), int(mxloc[1]+y)]
        cr = int(mx)
        
        # ignore small radiuses
        if cr < 6: continue

        # accumulate average R, G, B for circular object
        ar, ag, ab = get_mean_object_rgb_color(img, cx, cy, cr)
        sumr, sumg, sumb = sumr + ar, sumg + ag, sumb + ab

        # accumulate average H, S, V for circular object
        ah, aas, av = get_mean_object_hsv_color(hsv, cx, cy, cr)
        sumh, sums, sumv = sumh + ah, sums + aas, sumv + av

        # basic classification rules
        if (ag > 35):
            # class 2: too green - not developed
            objecttype = 2
        else:
            # class 1: this obeject is ok
            objecttype = 1
            
        # store object and its attributes
        objects.append(DetectedObject(cx, cy, h, w, cr, objecttype, ar, ag, ab, ah, aas, av))
    return objects, RGB(sumr, sumg, sumb), HSV(sumh, sums, sumv)

def apply_color_rules(objects, avgrgb, avghsv, dom):
    """Apply color rules to objects
    
    Modify classes of objects according to color.
    
    Special classes (6,7,8,9,11) created for debug purposes, should be
    changed to 1, 2 or -1 in final version
    
    Args:
        objects:        list of DetectedObjects
        avgrgb (RGB):   average R, G, B of objects
        avghsv (HSV):   average H, S, V of objects
        dom (RGB):      average R, G, B of objects
    """
   
    for obj in objects:
        if (obj.g - dom.g > -210):
            # object green corrected by dominant image green color is too big
            # should be class 2 
            if (obj.objtype != 2): obj.objtype = 6
            continue
        elif (obj.s - avghsv.s < -65):
            # object saturation corrected by average saturation is too low
            # should be class 2 
            if (obj.objtype != 2): obj.objtype = 7
            continue
        elif (obj.b - avgrgb.b > 23):
            # object blue corrected by average blue is too high
            # should be class 2 
            if (obj.objtype != 2): obj.objtype = 8
            continue
        elif (obj.g - avgrgb.g > 23):
            # object green corrected by average green is too high
            # should be class 2 
            if (obj.objtype != 2): obj.objtype = 9
            continue
        else:
            # otherwise
            # sould be class 1 
            if (obj.objtype != 1): obj.objtype = 11
            continue

def apply_diameter_rules(objects, medianradius):
    """Apply diameter rules to objects
    
    Modify classes of objects according to diameter.
    
    Special classes (3,4,) created for debug purposes, should be
    changed to 1, 2 or -1 in final version
    class -1: remove this object
    """
    
    for obj in objects:
        x = obj.radius / (1.0 * medianradius)

        # object is too large for class 2
        if (x > 1.0) and (obj.objtype == 2): obj.objtype = 1
        
        # too small
        if x < 0.5: obj.objtype = 3
        
        # too large
        if x > 2.0: obj.objtype = 4
        
        # extremly large - remove this object
        if x > 4.0: obj.objtype = -1    

def save_object_images(objects, outimg, img, dst, fname):
    """Save object images
    
    Save images of detected objects to directories according to object category.
    """
    height, width = img.shape[:2]
    i = 1        
    for obj in objects:
        sw = 20
        left, right = int(max(obj.y-obj.height/2-sw, 0)), int(min(obj.y+obj.height/2+sw, height))        
        top, bottom = int(max(obj.x-obj.width/2-sw, 0)), int(min(obj.x+obj.width/2+sw, width))        

        # get subimage with object
        subimg = img[left:right, top:bottom]

        # modify classes of objects - asymmetry based rules
        obj.asymmetry = get_asymmetry(subimg, sw)
        if (obj.asymmetry > 650):
            obj.objtype = 5
        
        dstdir2 = "./" + dst + "/objects"
        writeObjects = True
        if True:
            if (obj.objtype == 0):
                #red - not used
                cv2.circle(outimg, (obj.x, obj.y), obj.radius+4,(0,0,255),2)
                if writeObjects:
                    cv2.imwrite(dstdir2 + "/0/" + fname + "-" + str(i) + ".png", subimg)
            elif (obj.objtype == 1):
                # blue - developed
                cv2.circle(outimg, (obj.x, obj.y), obj.radius+4,(255,100,0),2)
                if writeObjects:
                    cv2.imwrite(dstdir2 + "/1/" + fname + "-" + str(i) + ".png", subimg)
            elif (obj.objtype == 2):
                # green - not developed
                cv2.circle(outimg, (obj.x, obj.y), obj.radius+4,(0,255,0),2)
                if writeObjects:
                    cv2.imwrite(dstdir2 + "/2/" + fname + "-" + str(i) + ".png", subimg)
            elif (obj.objtype == 3):
                # yellow - too small
                cv2.circle(outimg, (obj.x, obj.y), obj.radius+4,(0,255,255),2)
                if writeObjects:
                    cv2.imwrite(dstdir2 + "/3/" + fname + "-" + str(i) + ".png", subimg)
            elif (obj.objtype == 4):
                # purple - too large
                cv2.circle(outimg, (obj.x, obj.y), obj.radius+4,(255,0,255),2)
                if writeObjects:
                    cv2.imwrite(dstdir2 + "/4/" + fname + "-" + str(i) + ".png", subimg)
            elif (obj.objtype == 5):
                # cyan - too asymmetric
                cv2.circle(outimg, (obj.x, obj.y), obj.radius+4,(255,255,0),2)
                if writeObjects:
                    cv2.imwrite(dstdir2 + "/5/" + fname + "-" + str(i) + ".png", subimg)
            elif (obj.objtype == 11):
                # white - corrected by pass 2 (color based rules), should be class 1
                cv2.circle(outimg, (obj.x, obj.y), obj.radius+4,(255,255,0),2)
                if writeObjects:
                    cv2.imwrite(dstdir2 + "/11/" + fname + "-" + str(i) + ".png", subimg)
            else:
                # black - corrected by pass 2 (color based rules), should be class 2
                cv2.circle(outimg, (obj.x, obj.y), obj.radius+4,(0,0,0),4)
                if writeObjects:
                    cv2.imwrite(dstdir2 + "/" + str(obj.objtype) + "/" + fname + "-" + str(i) + ".png", subimg)
        i += 1    

def save_object_info(dstdir, fname, objects, outimg, temperature, medianradius, avgrgb, avghsv, dom, ftotal):
    """Save object info
    
    Save all info about detected objects to  global txt file (ftotal).
    Add object numbers to final analyzed image.
    
    Args:
        dstdir:        destination directory
        fname:         file name
        objects:       list of DetectedObjects
        outimg:        image for output
        temperature:   temperature (from directory name)
        medianradius:  median of object radius
        avgrgb (RGB):  average R, G, B of objects
        avghsv (HSV):  average H, S, V of objects
        dom (RGB):     dominant color        
        ftotal:        file for complete results
    """
    
    header_fields = ("temperature", "filename", "directory", "ID", "x", "y", "radius", "radius/avgradius",
              "red", "green", "blue", "hue", "saturation", "value", 
              "avg_red", "avg_green", "avg_blue", "avg_hue", "avg_saturation", "avg_value",
              "dominant_red", "dominant_green", "dominant_blue",
              "red-avg_red", "green-avg_green", "blue-avg_blue",
              "hue-avg_hue", "saturation-avg_saturation", "value-avg_value",
              "red-dominant_red", "green-dominant_green", "blue-dominant_blue",
              "medianradius", "asymmetry",
               "type", "typetxt");
    header = "\t".join(header_fields);

    f = open(os.path.join(dstdir, fname + ".txt"),'w')
    f.write(header + "\n");

    cnt = 0
    for obj in objects:
        output = (temperature, fname, dstdir, str(cnt), str(obj.x), str(obj.y), str(obj.radius), str(round(100 * obj.radius/medianradius, 2)),
                  str(obj.r), str(obj.g), str(obj.b), str(obj.h), str(obj.s), str(obj.v),
                  str(avgrgb.r), str(avgrgb.g), str(avgrgb.b), str(avghsv.h), str(avghsv.s), str(avghsv.v),
                  str(dom.r), str(dom.g), str(dom.b),
                  str(obj.r - avgrgb.r), str(obj.g - avgrgb.g), str(obj.b - avgrgb.b),
                  str(obj.h - avghsv.h), str(obj.s - avghsv.s), str(obj.v - avghsv.v),
                  str(obj.r - dom.r), str(obj.g - dom.g), str(obj.b - dom.b),
                  str(medianradius), str(obj.asymmetry),
                  str(obj.objtype), "type" + str(obj.objtype));
        f.write("\t".join(output) + "\n");
        ftotal.write("\t".join(output) + "\n");

        # draw number of object into the image
        cv2.putText(outimg, str(cnt), (obj.x+20, obj.y+20), 0, 0.4, (255, 255, 255), 3, 8)
        cv2.putText(outimg, str(cnt), (obj.x+20, obj.y+20), 0, 0.4, (0, 0, 0), 1, 8)
        cnt += 1
    f.write("\n")
        
    f.close()    

def save_analyzed_image(dstdir, fname, img):
    """Save analyzed image"""

    print(os.path.join(dstdir, fname + "-analyzed.jpg"))
    cv2.imwrite(os.path.join(dstdir, fname + "-analyzed.jpg"), img)

def calculate_statistics(objects, sumrgb, sumhsv):
    """Calculate statistics for objects

    Calculate average and median radius and average object color values  
    Args:
        objects:        list of DetectedObjects
        sumrgb (RGB):   sum of r, g, b values for all objects
        sumhsv (HSV):   sum of h, s, v values for all objects
    Returns:
        avgradius:      average object radius
        medianradius:   median object radius
        avgrgb (RGB):   average rgb value
        avghsv (HSV:    average hsv value
    """
    
    radiuses = [obj.radius for obj in objects]
    avgradius = stat.mean(radiuses)
    medianradius = stat.median(radiuses)
    l = len(objects)
    avgrgb = RGB(sumrgb.r / l, sumrgb.g / l, sumrgb.b / l)
    avghsv = HSV(sumhsv.h / l, sumhsv.s / l, sumhsv.v / l)   
    return avgradius, medianradius, avgrgb, avghsv

def analyze_image(fname, root, dstdir, dst, temperature, ftotal):
    """Analyze one image
    
    Args:
        fname:  filename e.g. "image1.jpg"
        root:   directory, where the input image is located
        dstdir: destination directory full name
        dst:    destination directory short name 
        temperature: temperature (from directory name)
        ftotal: file for complete results       
    """

    # read image, preprocess, extract mask
    origimg = cv2.imread(os.path.join(root, fname))
    outimg, wbimg = preprocess_image(origimg)
    dom = get_dominant_image_color(wbimg)
    bw, hsv = extract_mask(wbimg)

    # extract objects & statistics
    dist, contours, peaks8u = extract_contours(bw)
    objects, sumrgb, sumhsv = extract_objects(contours, dist, peaks8u, wbimg, hsv)
    avgradius, medianradius, avgrgb, avghsv = calculate_statistics(objects, sumrgb, sumhsv)

    # apply class rules to objects 
    apply_color_rules(objects, avgrgb, avghsv, dom)
    apply_diameter_rules(objects, medianradius)

    # save results
    save_object_images(objects, outimg, origimg, dst, fname)
    save_object_info(dstdir, fname, objects, outimg, temperature, medianradius, avgrgb, avghsv, dom, ftotal)
    save_analyzed_image(dstdir, fname, outimg)

def traverse_dir(root, dname, dstdir, dst, ftotal):
    """Process one directory
    
    Args:
        root:   root directory name
        dname:  subdirectory name
        dstdir: destination directory full name
        dst:    destination directory short name
        ftotal: file for complete results        
    """
    fullpath = os.path.join(root, dname)

    # get temperature from first level directory name
    temperature = root.split(os.path.sep)[2]
    
    for root, dirs, files in os.walk(fullpath):    
        for fname in files:
            # lets analyze this image
            analyze_image(fname, root, dstdir, dst, temperature, ftotal)

def traverse_data(src, dst, ftotal):
    """Traverse directory structure ./[temperature]/[id]/[*.jpg]
    
    Args:
        src: source directory
        dst: destination directory
        ftotal: file for complete results
    """
    for root, dirs, files in os.walk(src):
        num_sep = root.count(os.path.sep)
        # for second level of subdirectories:
        if num_sep >= 2:
            for dname in dirs:
                dstname = os.path.join(root, dname)
                spl = dstname.split(os.path.sep)
                spl[1] = dst
                destdir = os.path.join(*spl)
                print(destdir)
                # create output subdirectory
                os.makedirs(destdir)
                # process one directory
                traverse_dir(root, dname, destdir, dst, ftotal)

def clear_output_directory(dst):
    try:
        os.stat(dst)
        shutil.rmtree('./' + dst)
    except:
        pass

def create_output_class_directories(dst):
    os.mkdir('./' + dst) 
    os.mkdir('./' + dst + '/objects/') 
    os.mkdir('./' + dst + '/objects/0/') 
    os.mkdir('./' + dst + '/objects/1/') 
    os.mkdir('./' + dst + '/objects/2/') 
    os.mkdir('./' + dst + '/objects/3/') 
    os.mkdir('./' + dst + '/objects/4/') 
    os.mkdir('./' + dst + '/objects/5/') 
    os.mkdir('./' + dst + '/objects/6/') 
    os.mkdir('./' + dst + '/objects/7/') 
    os.mkdir('./' + dst + '/objects/8/') 
    os.mkdir('./' + dst + '/objects/9/') 
    os.mkdir('./' + dst + '/objects/10/') 
    os.mkdir('./' + dst + '/objects/11/') 

def main():
    """Process all data
    
    Expected directory structure: ./[temperature]/[id]/[*.jpg]
    Example:                      ./30/30_2/13_30_025_13 (4).jpg
    """

    # source directory (relative to current path)
    src = './data'

    # destination directory (relative to current path)
    dst = 'output'

    clear_output_directory(dst)
    create_output_class_directories(dst)

    # text output - attributes for all images are stored here
    ftotal = open('./' + dst + '/total.txt','w')

    # header for output
    header_fields = ("temperature", "filename", "directory", "ID", "x", "y", "radius", "radius/avgradius",  
              "red", "green", "blue", "hue", "saturation", "value", 
              "avg_red", "avg_green", "avg_blue", "avg_hue", "avg_saturation", "avg_value",
              "dominant_red", "dominant_green", "dominant_blue",
              "red-avg_red", "green-avg_green", "blue-avg_blue",
              "hue-avg_hue", "saturation-avg_saturation", "value-avg_value",
              "red-dominant_red", "green-dominant_green", "blue-dominant_blue",
              "medianradius", "asymmetry", 
              "type", "typetxt");
    header = "\t".join(header_fields);
    ftotal.write(header + "\n");

    # let's do it!
    traverse_data(src, dst, ftotal)

    ftotal.close()

if __name__ == "__main__":
    main()

