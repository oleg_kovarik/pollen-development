# Pollen development classification

Projects contains script for extraction and classification of stained pollen grains 
from the light microscope images. 

## Usage

1.  Clone this repository
2.  Install dependencies
	- Python >= 3
	- OpenCV 3 or 4
3.  Run demo

    > python pollen-development-demo.py

## Data & classes

Extracted objects are classified as a developed/not developed classes.

### Input
	Expected directory structure: ./data/[temperature]/[id]/[*.jpg]
	Example:                      ./data/30/30_2/13_30_025_13 (4).jpg

### Output
	./output/objects/	all extracted objects sorted by class
	./output/[temperature]/[id]/	analyzed images + txt info
	./output/total.txt	info on all images

In addition to main classes 1 & 2, there are several classes with problematic objects, which should be reviewed and used for parameter tuning.

### Classes
	0 red		not used
	
	1 blue		DEVELOPED
	2 green		NOT DEVELOPED
	
	3 yellow	object too small
	4 purple	object too large
	5 cyan		object too asymmetric
	
	6, 7, 8, 9 black	should be class 2 (color based rules)
	
	10			not used
	11 white	should be class 1 (color based rules)

### Parameter tuning
Can be done by changing threshold values in following methods:

	apply_color_rules
	apply_diameter_rules
	save_object_images (asymmetry)

## Method

**1. Object segmentation**

	- Image preprocessing
	- HSV thresholding
	- Extract contours (fill holes)
	- Extract peaks
	- Extract circular objects

**2. Feature extraction**

    - Average RGB, HSV
    - Dominant color
    - Diameter

**3. Object classification**

    - color based rules
    - diameter based rules
    - asymmetry based rules

## Example

### Original image
![Original image](./img/sample-original.jpg)

### White balanced image
![White balanced image](./img/sample-white_balanced.jpg)

### HSV threshold
![HSV threshold](./img/sample-hsv_threshold.jpg)

### Contour extraction (fill holes)
![Contour extraction (fill holes)](./img/sample-contours.jpg)

### Peaks
![Peaks](./img/sample-peaks.jpg)

### Result
![Result](./img/sample-result.jpg)

### Developed
![Developed](./img/sample-objects_developed.jpg)

### Not developed
![Not developed](./img/sample-objects_nondeveloped.jpg)

## Contact

oleg.kovarik@gmail.com
